#imagen base
FROM node:latest

#Directorio de la app
WORKDIR /frontTechuFinal

#Copia de archivos
ADD /build/default /frontTechuFinal/build/default

ADD bower.json /app

#Dependencias
RUN npm start

#Puerto que expongo
EXPOSE 3000

#Comando
CMD ["npm","start"]
