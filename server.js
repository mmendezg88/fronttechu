/**
* URL's principal de las API's del back-end en Node.js
*/

var url = "http://localhost:3001/movements/V00";

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  //res.header("Access-Control-Allow-Origin","*");
  //res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  // res.header("Access-Control-Allowed-Origin","*");
  // res.header("Access-Control-Allowed-Headers","Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Access-Control-Allow-Request-Method");
  // res.header("Access-Control-Allowed-Methods","GET, POST, OPTIONS, PUT, DELETE");
  // res.header("Allow","GET, POST, OPTIONS, PUT, DELETE");
  next();
});

var requestjson = require('request-json');
var path = require('path');
app.use(express.static(__dirname + '/build/default'));
console.log(__dirname+'/build/default');
app.listen(port);

console.log('Encapsulando polymer en node: ' + port);

app.get('*',function(req, res){
    res.sendFile("index.html",{root:'.'});
    //JSON CON 3 CLIENTES FICTICIOS
});
